import React, { useState } from "react";
import { View, TextInput, Button, StyleSheet, Text, Modal, TouchableOpacity } from "react-native";
import { AntDesign } from "@expo/vector-icons";
import apiFunctions from './api';

const RegisterScreen = ({ navigation }) => {
  const [NomeUsuario, setNomeUsuario] = useState('');
  const [Email, setEmail] = useState('');
  const [CPF, setCPF] = useState('');
  const [Senha, setSenha] = useState('');
  const [modalVisible, setModalVisible] = useState(false);
  const [modalTitle, setModalTitle] = useState('');
  const [modalMessage, setModalMessage] = useState('');

  const showAlert = (title, message) => {
    setModalTitle(title);
    setModalMessage(message);
    setTimeout(() => setModalVisible(true), 100);
  };

  const handleRegister = async () => {
    try {
      const response = await apiFunctions.criarUsuario({ NomeUsuario, Email, CPF, Senha });

      if (response.status === 201) {
        showAlert('Sucesso', 'Usuário cadastrado com sucesso.');
        navigation.navigate('Home');
      } else {
        showAlert('Erro', response.data.error);
      }
    } catch (error) {
      console.error('Erro ao cadastrar:', error);
      showAlert('Erro', 'Algo deu errado. Tente novamente mais tarde.');
    }
  };

  return (
    <View style={styles.container}>
      <Modal
        animationType="slide"
        transparent={true}
        visible={modalVisible}
        onRequestClose={() => {
          setModalVisible(!modalVisible);
        }}
      >
        <View style={styles.centeredView}>
          <View style={styles.modalView}>
            <Text style={styles.modalTitle}>{modalTitle}</Text>
            <Text style={styles.modalText}>{modalMessage}</Text>
            <Button
              onPress={() => setModalVisible(!modalVisible)}
              title="OK"
              color="#3e945a"
            />
          </View>
        </View>
      </Modal>
      <View>
        <Text style={styles.title}>INSIRA SUAS INFORMAÇÕES DE</Text>
        <Text style={[styles.title, styles.subtitle]}>CADASTRO</Text>
      </View>
      <TextInput
        style={styles.input}
        placeholder="Nome"
        onChangeText={setNomeUsuario}
        value={NomeUsuario}
      />
      <TextInput
        style={styles.input}
        placeholder="Email"
        onChangeText={setEmail}
        value={Email}
      />
      <TextInput
        style={styles.input}
        placeholder="CPF"
        onChangeText={setCPF}
        value={CPF}
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        secureTextEntry={true}
        onChangeText={setSenha}
        value={Senha}
      />
      <View style={styles.buttonContainer}>
        <Button
          title="CADASTRAR"
          onPress={handleRegister}
          color='#3e945a'
        />
      </View>

      <TouchableOpacity style={styles.backButton} onPress={() => navigation.navigate("PaginaInicial")}>
        <AntDesign name="arrowleft" size={24} color="#3e945a" />
      </TouchableOpacity>

      <View style={styles.footer}>
        <Text style={styles.footerText}>© 2024 Reserva de Quadras Esportivas</Text>
      </View>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#FFFFFF',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  title: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 1,
  },
  subtitle: {
    textAlign: 'center',
    fontSize: 16,
    fontWeight: 'bold',
  },
  input: {
    borderColor: '#4dbf72',
    borderWidth: 3,
    borderRadius: 30,
    fontSize: 12,
    width: '80%',
    padding: 10,
    marginVertical: 3,
    marginTop: 3,
    marginBottom: 5,
    paddingHorizontal: 10,
    height: 40,
  },
  buttonContainer: {
    backgroundColor: '#e6e6e6',
    borderRadius: 20,
    overflow: 'hidden',
    width: '80%',
    marginBottom: 3,
    marginVertical: 15,
    marginTop: 4,
  },
  footer: {
    position: 'absolute',
    bottom: 0,
  },
  footerText: {
    fontSize: 14,
    color: '#666',
  },
  centeredView: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    marginTop: 22
  },
  modalView: {
    margin: 20,
    backgroundColor: "white",
    borderRadius: 20,
    padding: 35,
    alignItems: "center",
    shadowColor: "#000",
    shadowOffset: {
      width: 0,
      height: 2
    },
    shadowOpacity: 0.25,
    shadowRadius: 4,
    elevation: 5
  },
  modalTitle: {
    marginBottom: 15,
    textAlign: "center",
    fontWeight: "bold",
    fontSize: 20,
    color: "#3e945a"
  },
  modalText: {
    marginBottom: 15,
    textAlign: "center",
    fontSize: 16,
  },
  backButton: {
    position: "absolute",
    top: 40,
    left: 20,
    zIndex: 999,
  },
});

export default RegisterScreen;
